# Automatic SemVer 2.0 Versioning for .NET Builds

Provides MSBuild targets that hook into the .NET build process to automatically generate a
[SemVer 2.0](https://semver.org/spec/v2.0.0.html)-compatible version based on the current repository revision.

Supports multiple VCS. The default implementation is compatible with common workflows such as Git/Hg Flow or GitHub Flow.
Through several MSBuild injection points the process how version numbers are derived can be influenced to match custom workflows.



# Usage

## Configuration

Set a version prefix in your project file and define the repository type:
```xml
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <VersionPrefix>1.2.0</VersionPrefix>
    <RepositoryType>git</RepositoryType>
  </PropertyGroup>
</Project>
```

The ***version prefix*** is expected to be of format `X.Y.Z`, with a major `X`, minor `Y` and patch version number `Z`.
If the patch or patch and minor version numbers are omitted, they default to `0`.
Additional characters in the version prefix are ignored and will be removed automatically.

The ***repository type*** controls what VCS engine is used to retrieve further information on the repository.
If no repository type is defined, the repository type is auto-detected by searching for a corresponding source control
system metadata-folder in the project root folder.


## Activation

To activate automated versioning, add the following package reference to the project file:
```xml
<Project Sdk="Microsoft.NET.Sdk">
  <ItemGroup>
    <PackageReference Include="XploRe.Versioning.Tools.DotNet" Version="0.4.0" PrivateAssets="All" />
  </ItemGroup>
</Project>
```

Setting `PrivateAssets` to `All` ensures that the package reference is used for building only and will not appear as a
dependency in the .nuspec file of the project.
If a tool is used to add the dependency, the `PrivateAssets` property should be set automatically.

During each build, the assembly and package version numbers will now automatically include a [SemVer 2.0](https://semver.org/spec/v2.0.0.html)-compatible tag and build number for development and release candidate builds, as well as build metadata.


# Documentation
For the full documentation please visit the repository at: \
https://gitlab.com/xplo-re/dotnet/versioning.tools.dotnet



# License

Released under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Copyright © xplo.re IT Services, Michael Maier. \
All rights reserved.
