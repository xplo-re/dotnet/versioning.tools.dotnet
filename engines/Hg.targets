<?xml version="1.0" encoding="utf-8" ?>
<!--
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0.
 * See bundled LICENSE.txt for license information.
 -->
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003"
         TreatAsLocalProperty="_HgRepositoryTags">

  <!-- Configuration defaults. -->
  <PropertyGroup>
    <RepositoryRevisionIdMaxLength Condition=" '$(RepositoryRevisionIdMaxLength)' == '' ">12</RepositoryRevisionIdMaxLength>
  </PropertyGroup>

  <!-- Mercurial executable. -->
  <Choose>
    <When Condition=" '$(MercurialToolExe)' == '' ">
      <Choose>
        <When Condition="$([MSBuild]::IsOSPlatform(`Windows`))">
          <PropertyGroup>
            <MercurialToolExe Condition=" '$(MercurialToolPath)' == '' ">hg.exe</MercurialToolExe>
            <MercurialToolExe Condition=" '$(MercurialToolPath)' != '' ">$([MSBuild]::EnsureTrailingSlash(`$(MercurialToolPath)`))hg.exe</MercurialToolExe>
          </PropertyGroup>
        </When>
        <Otherwise>
          <PropertyGroup>
            <MercurialToolExe Condition=" '$(MercurialToolPath)' == '' ">hg</MercurialToolExe>
            <MercurialToolExe Condition=" '$(MercurialToolPath)' != '' ">$([MSBuild]::EnsureTrailingSlash(`$(MercurialToolPath)`))hg</MercurialToolExe>
          </PropertyGroup>
        </Otherwise>
      </Choose>
    </When>
  </Choose>

  <!-- Extract Mercurial repository properties. -->
  <Target Name="_VersioningHgGetRepositoryProperties">

    <!-- Mercurial has native support for local revision numbers, that can be globally unique in the context of a central
         server to build releases. -->
    <Exec Condition=" '$(RepositoryRevisionNumber)' == '' "
          Command="$(MercurialToolExe) identify --rev=. --num"
          EchoOff="true" StandardOutputImportance="low"
          ConsoleToMsBuild="true" StdOutEncoding="UTF-8">
      <Output TaskParameter="ConsoleOutput" PropertyName="RepositoryRevisionNumber" />
    </Exec>

    <!-- Fetch the full-length Mercurial working directory parent hash identifier.
         The default via `identify` is truncated to 12 characters. -->
    <Exec Condition=" '$(RepositoryRevisionId)' == '' "
          Command="$(MercurialToolExe) log --rev=. --template '{node}'"
          EchoOff="true" StandardOutputImportance="low"
          ConsoleToMsBuild="true" StdOutEncoding="UTF-8">
      <Output TaskParameter="ConsoleOutput" PropertyName="RepositoryRevisionId" />
    </Exec>

    <!-- Identify the current branch name. -->
    <Exec Condition=" '$(RepositoryBranch)' == '' "
          Command="$(MercurialToolExe) identify --rev=. --branch"
          EchoOff="true" StandardOutputImportance="low"
          ConsoleToMsBuild="true" StdOutEncoding="UTF-8">
      <Output TaskParameter="ConsoleOutput" PropertyName="RepositoryBranch" />
    </Exec>

    <!-- Fetch tags. If a revision has multiple tags associated, all tags are listed, separated by whitespace, in reverse
         order that the tags were associated with the revision. -->
    <Exec Command="$(MercurialToolExe) identify --rev=. --tags"
          EchoOff="true" StandardOutputImportance="low"
          ConsoleToMsBuild="true" StdOutEncoding="UTF-8">
      <Output TaskParameter="ConsoleOutput" PropertyName="_HgRepositoryTags" />
    </Exec>

    <ItemGroup>
      <!-- Convert space-separated list of tags into an item group. -->
      <RepositoryTags Include="$([System.Text.RegularExpressions.Regex]::Split(`$([MSBuild]::Escape('$(_HgRepositoryTags.Trim())'))`, `\s+`))" />
    </ItemGroup>

    <!-- Set commonly known repository properties from properties of this engine. -->
    <PropertyGroup>
      <!-- Full-length (unabbreviated) unique Mercurial commit hash identifier. -->
      <RepositoryCommit Condition=" '$(RepositoryCommit)' == '' And '$(RepositoryRevisionId)' != '' ">$(RepositoryRevisionId)</RepositoryCommit>
    </PropertyGroup>

  </Target>

</Project>
